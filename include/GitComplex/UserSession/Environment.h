/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/UserSession/Namespace.h>
#include <GitComplex/Core/Qt/Path.h>
#include <GitComplex/Settings/Storage.h>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/UserSession/Environment.h
 * \brief include/GitComplex/UserSession/Environment.h
 */

namespace GitComplex::UserSession
{

/*!
 * \class Environment
 * \brief User session environment.
 *
 * \headerfile GitComplex/UserSession/Environment.h
 *
 * User session environment.
 *
 * \note [[Abstraction::Detail]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        Environment
{
public:
    Environment();

    Settings::UserStorage       userStorage() const;
    Settings::RepositoryStorage repositoryStorage() const;
    Path workingDirRepository() const { return m_working_dir_repository; }

private:
    Path m_repository_storage_file;
    Path m_working_dir_repository;
};

} // namespace GitComplex::UserSession
