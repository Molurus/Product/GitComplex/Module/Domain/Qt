/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Shell/Command/Std/Summary.h>
#include <GitComplex/Shell/Command/Qt/Summary.h>
#include <GitComplex/Shell/Command/Converter/Output.h>

#include <GitComplex/Core/Converter/Path.h>

/*!
 * \file include/GitComplex/Shell/Command/Converter/Summary.h
 * \brief include/GitComplex/Shell/Command/Converter/Summary.h
 */

namespace GitComplex::Converter
{

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
template <class From>
inline
auto convert(From&& summary,
             To<Shell::Command::Qt::Summary> = {},
             RequireSame<From, Shell::Command::Std::Summary> = {})
{
    using namespace Shell::Command::Qt;
    return Summary{to<Line>(ForwardAs<From>(summary.command)),
                   to<Core::Qt::Path>(ForwardAs<From>(summary.workingDirectory)),
                   summary.error,
                   summary.exitStatus,
                   to<Output>(        ForwardAs<From>(summary.output))};
}

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
template <class From>
inline
auto convert(From&& summaries,
             To<Shell::Command::Qt::SummaryList> = {},
             RequireSame<From, Shell::Command::Std::SummaryList> = {})
{
    using namespace Shell::Command::Qt;
    SummaryList to_summaries;

    for (const auto& summary:summaries)
        to_summaries.push_back(to<Summary>(ForwardAs<From>(summary)));

    return to_summaries;
}

} // namespace GitComplex::Converter
