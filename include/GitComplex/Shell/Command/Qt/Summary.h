/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Shell/Command/Qt/Namespace.h>
#include <GitComplex/Shell/Command/Qt/Line.h>
#include <GitComplex/Shell/Command/Qt/ExitStatus.h>
#include <GitComplex/Shell/Command/Qt/Output.h>
#include <GitComplex/Core/Qt/Path.h>

#include <vector>

/*!
 * \file include/GitComplex/Shell/Command/Qt/Summary.h
 * \brief include/GitComplex/Shell/Command/Qt/Summary.h
 */

namespace GitComplex::Shell::Command::Qt
{

/*!
 * \struct Summary
 * \brief Summary.
 *
 * \headerfile GitComplex/Shell/Command/Qt/Summary.h
 *
 * Summary.
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Qt]]
 */
struct Summary
{
    Line            command;
    Path            workingDirectory;
    std::error_code error;
    ExitStatus      exitStatus;
    Output          output;
};

/*!
 * List of \ref Command::Qt::Summary "Summary".
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Qt]]
 */
using SummaryList = std::vector<Summary>;

} // namespace GitComplex::Shell::Command::Qt
