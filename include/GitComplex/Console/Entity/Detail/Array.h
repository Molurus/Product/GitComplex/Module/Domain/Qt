/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/Entity/Single.h>
#include <GitComplex/Console/Entity/EventDelegate/Array.h>
#include <GitComplex/Settings/Utility.h>

#include <otn/all.hpp>

#include <array>

/*!
 * \file include/GitComplex/Console/Entity/Detail/Array.h
 * \brief include/GitComplex/Console/Entity/Detail/Array.h
 */

namespace GitComplex::Console::Entity::Detail
{

/*!
 * \class Array
 * \brief Array.
 *
 * \headerfile GitComplex/Console/Entity/Detail/Array.h
 *
 * Array.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <std::size_t ConsoleCount>
class Array
{
public:
    using UniqueConsole = otn::slim::unique_single<Single>;
    using Consoles      = std::array<UniqueConsole, ConsoleCount>;

    explicit Array(Consoles consoles)
        : m_consoles{std::move(consoles)},
          m_event_delegate{otn::itself}
    {
        for (std::size_t i = 0; i < m_consoles.size(); ++i)
            QObject::connect(&console(i).commandExecutionRequested(),
                             &EventDelegate::Single::commandExecutionRequested,
                             [this, i](const Command::Line& command)
                             { (*m_event_delegate).commandExecutionRequested(i, command); });
    }

    Command::Line currentCommand(std::size_t console_index) const
    { return console(console_index).currentCommand(); }

    void printCommandSummaries(std::size_t console_index,
                               const Command::SummaryList& summaries)
    { console(console_index).printCommandSummaries(summaries); }

    void clearOutput(std::size_t console_index)
    { console(console_index).clearOutput(); }

    void adjustHasTarget(std::size_t console_index, bool has_target)
    { console(console_index).adjustHasTarget(has_target); }

    EventDelegate::Array& eventDelegate() const
    { return *m_event_delegate; }

    void save(Settings::RepositoryStorage& storage) const
    {
        storage.beginWriteArray("Console", m_consoles.size());
        for (std::size_t i = 0; i < m_consoles.size(); ++i)
        {
            storage.setArrayIndex(i);
            Settings::save_pmr(console(i), storage);
        }
        storage.endArray();
    }

    void load(Settings::RepositoryStorage& storage)
    {
        storage.beginReadArray("Console");
        for (std::size_t i = 0; i < m_consoles.size(); ++i)
        {
            storage.setArrayIndex(i);
            Settings::load_pmr(console(i), storage);
        }
        storage.endArray();
    }

protected:
    Single&       console(std::size_t index)
    { return *m_consoles.at(index); }
    const Single& console(std::size_t index) const
    { return *m_consoles.at(index); }

private:
    Consoles m_consoles;
    otn::raw::unique_single<EventDelegate::Array> m_event_delegate;
};

} // namespace GitComplex::Console::Entity::Detail
