/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/Entity/Array.h>
#include <GitComplex/Console/Entity/Detail/Array.h>
#include <GitComplex/Settings/Storable.h>

/*!
 * \file include/GitComplex/Console/Entity/Concrete/Array.h
 * \brief include/GitComplex/Console/Entity/Concrete/Array.h
 */

namespace GitComplex::Console::Entity::Concrete
{

/*!
 * \class Array
 * \brief Array.
 *
 * \headerfile GitComplex/Console/Entity/Concrete/Array.h
 *
 * Array.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <std::size_t ConsoleCount>
class Array final : public Entity::Array,
                    public Settings::RepositoryStorable
{
public:
    using Detail        = Entity::Detail::Array<ConsoleCount>;
    using UniqueConsole = typename Detail::UniqueConsole;
    using Consoles      = typename Detail::Consoles;

    explicit Array(Consoles consoles)
        : m_detail{std::move(consoles)}
    {}

private:
    Command::Line currentCommand(std::size_t console_index) const override
    { return m_detail.currentCommand(console_index); }

    void printCommandSummaries(std::size_t console_index,
                               const Command::SummaryList& summaries) override
    { m_detail.printCommandSummaries(console_index, summaries); }

    void clearOutput(std::size_t console_index) override
    { m_detail.clearOutput(console_index); }

    void adjustHasTarget(std::size_t console_index, bool has_target) override
    { m_detail.adjustHasTarget(console_index, has_target); }

    EventDelegate::Array& commandExecutionRequested() const override
    { return m_detail.eventDelegate(); }

    void save(Settings::RepositoryStorage& storage) const override { m_detail.save(storage); }
    void load(Settings::RepositoryStorage& storage) override       { m_detail.load(storage); }

private:
    Detail m_detail;
};

} // namespace GitComplex::Console::Entity::Concrete
