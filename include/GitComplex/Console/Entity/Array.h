/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Console/Entity/EventDelegate/Array.h>
#include <GitComplex/Shell/Command/Qt/Summary.h>

/*!
 * \file include/GitComplex/Console/Entity/Array.h
 * \brief include/GitComplex/Console/Entity/Array.h
 */

namespace GitComplex::Console::Entity
{

/*!
 * \class Array
 * \brief Array.
 *
 * \headerfile GitComplex/Console/Entity/Array.h
 *
 * Array.
 *
 * \note [[Abstraction::Interface]] [[Viper::Entity]] [[Framework::Qt]]
 */
class Array
{
public:
    using ArrayEventDelegate = EventDelegate::Array;

    virtual ~Array() = default;

    virtual Command::Line currentCommand(std::size_t console_index) const = 0;
    virtual void printCommandSummaries(std::size_t console_index,
                                       const Command::SummaryList& summaries) = 0;
    virtual void clearOutput(std::size_t console_index) = 0;

    virtual void adjustHasTarget(std::size_t console_index, bool has_target) = 0;

    virtual ArrayEventDelegate& commandExecutionRequested() const = 0;
};

} // namespace GitComplex::Console::Entity
