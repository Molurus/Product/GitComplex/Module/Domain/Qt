/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Entity/Qt/StatusProvider.h>
#include <GitComplex/Repository/Tree/Entity/Std/StatusProvider.h>

#include <QObject>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/Tree/Entity/Adapter/RelayStatusProvider.h
 * \brief include/GitComplex/Repository/Tree/Entity/Adapter/RelayStatusProvider.h
 */

namespace GitComplex::Repository::Tree::Entity::Adapter
{

/*!
 * \struct RelayStatusProvider
 * \brief RelayStatusProvider.
 *
 * \headerfile GitComplex/Repository/Tree/Entity/Adapter/RelayStatusProvider.h
 *
 * RelayStatusProvider.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        RelayStatusProvider final : public Qt::StatusProvider
{
public:
    RelayStatusProvider(
        otn::raw::weak_single<const Std::StatusProvider> std_status_provider,
        QObject& context)
        : m_std_status_provider{std::move(std_status_provider)},
          m_context{context}
    {}

private:
    using IdList = Repository::Entity::Qt::IdList;

    void requestStatus(
        Qt::IdList ids,
        ReceiveAction<Qt::StatusList> receive_action) const override;

    // Transmit methods.
    static void transmitStatuses(
        ReceiveAction<Qt::StatusList> receive_action,
        Std::StatusList statuses);

    otn::raw::weak_single<const Std::StatusProvider> m_std_status_provider;
    QObject& m_context;
};

} // namespace GitComplex::Repository::Tree::Entity::Adapter
