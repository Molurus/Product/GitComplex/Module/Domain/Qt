/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Entity/Std/Structure.h>
#include <GitComplex/Repository/Tree/Entity/Qt/Structure.h>

#include <GitComplex/Core/Converter/String.h>

/*!
 * \file include/GitComplex/Repository/Tree/Entity/Converter/Structure.h
 * \brief include/GitComplex/Repository/Tree/Entity/Converter/Structure.h
 */

namespace GitComplex::Converter
{

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
template <class From>
inline
auto convert(From&& structure,
             To<Repository::Tree::Entity::Qt::Structure> = {},
             RequireSame<From, Repository::Tree::Entity::Std::Structure> = {})
{
    using namespace Repository::Tree::Entity::Qt;
    Structure::List submodules =
        to<Structure::List>(ForwardAs<From>(structure.submodules));

    return Structure{ForwardAs<From>(structure.id),
                     to<Core::Qt::Path>(ForwardAs<From>(structure.path)),
                     structure.directoryState,
                     ForwardAs<From>(structure.superprojectId),
                     std::move(submodules)};
}

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
template <class From>
inline
auto convert(From&& structures,
             To<Repository::Tree::Entity::Qt::Structure::List> = {},
             RequireSame<From, Repository::Tree::Entity::Std::Structure::List> = {})
{
    using namespace Repository::Tree::Entity::Qt;
    Structure::List to_structures;

    for (auto& structure:structures)
        to_structures.push_back(to<Structure>(ForwardAs<From>(structure)));

    return to_structures;
}

} // namespace GitComplex::Converter
