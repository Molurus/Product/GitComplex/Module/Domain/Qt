/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Entity/Std/Status.h>
#include <GitComplex/Repository/Tree/Entity/Qt/Status.h>

#include <GitComplex/Core/Converter/String.h>

/*!
 * \file include/GitComplex/Repository/Tree/Entity/Converter/Status.h
 * \brief include/GitComplex/Repository/Tree/Entity/Converter/Status.h
 */

namespace GitComplex::Converter
{

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
template <class From>
inline
auto convert(From&& status,
             To<Repository::Tree::Entity::Qt::Status> = {},
             RequireSame<From, Repository::Tree::Entity::Std::Status> = {})
{
    using namespace Repository::Entity::Qt;
    using namespace Repository::Tree::Entity::Qt;
    return Status{ForwardAs<From>(status.id),
                  to<Branch>(ForwardAs<From>(status.branch)),
                  to<Sha>(   ForwardAs<From>(status.sha)),
                  status.hasModifications};
}

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
template <class From>
inline
auto convert(From&& from_statuses,
             To<Repository::Tree::Entity::Qt::StatusList> = {},
             RequireSame<From, Repository::Tree::Entity::Std::StatusList> = {})
{
    using namespace Repository::Tree::Entity::Qt;
    StatusList to_statuses;

    for (const auto& status:from_statuses)
        to_statuses.push_back(to<Status>(ForwardAs<From>(status)));

    return to_statuses;
}

} // namespace GitComplex::Converter
