/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Entity/Qt/StructureObservable.h>
#include <GitComplex/Repository/Tree/Entity/Qt/StatusUpdater.h>
#include <GitComplex/Core/Qt/ObjectConnection.h>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/Tree/Entity/Qt/Detail/StatusSynchronizer.h
 * \brief include/GitComplex/Repository/Tree/Entity/Qt/Detail/StatusSynchronizer.h
 */

namespace GitComplex::Repository::Tree::Entity::Qt::Detail
{

/*!
 * \class StatusSynchronizer
 * \brief StatusSynchronizer.
 *
 * \headerfile GitComplex/Repository/Tree/Entity/Qt/Detail/StatusSynchronizer.h
 *
 * StatusSynchronizer.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        StatusSynchronizer final
{
public:
    StatusSynchronizer(
        const StructureObservable& observable,
        otn::weak_optional<Entity::Qt::StatusUpdater> updater);

private:
    FixedObjectConnections<1> m_connections;
};

} // namespace GitComplex::Repository::Tree::Entity::Qt::Detail
