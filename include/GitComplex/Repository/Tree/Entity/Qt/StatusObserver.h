/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Tree/Entity/Qt/Status.h>

/*!
 * \file include/GitComplex/Repository/Tree/Entity/Qt/StatusObserver.h
 * \brief include/GitComplex/Repository/Tree/Entity/Qt/StatusObserver.h
 */

namespace GitComplex::Repository::Tree::Entity::Qt
{

/*!
 * \class StatusObserver
 * \brief StatusObserver.
 *
 * \headerfile GitComplex/Repository/Tree/Entity/Qt/StatusObserver.h
 *
 * StatusObserver.
 *
 * \note [[Abstraction::Interface]] [[Viper::Entity]] [[Framework::Qt]]
 */
class StatusObserver
{
public:
    virtual ~StatusObserver() = default;

    virtual void updateRepositoriesStatus(StatusList statuses) = 0;
};

} // namespace GitComplex::Repository::Tree::Entity::Qt
