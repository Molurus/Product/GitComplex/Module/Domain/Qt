/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Service/Qt/Registry.h>
#include <GitComplex/Repository/Service/Qt/Detail/Registry.h>
#include <GitComplex/Settings/Storable.h>

/*!
 * \file include/GitComplex/Repository/Service/Qt/Concrete/Registry.h
 * \brief include/GitComplex/Repository/Service/Qt/Concrete/Registry.h
 */

namespace GitComplex::Repository::Service::Qt::Concrete
{

/*!
 * \class Registry
 * \brief Registry.
 *
 * \headerfile GitComplex/Repository/Service/Qt/Concrete/Registry.h
 *
 * Registry.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class Registry final : public Qt::Registry,
                       public Settings::UserStorable
{
public:
    using Detail = Qt::Detail::Registry;

    Registry(otn::raw::weak_single<const Detail::StdIdProvider>       std_id_provider,
             otn::raw::weak_single<const Detail::StdStructureScanner> std_structure_scanner,
             otn::raw::weak_single<const Detail::StdStatusProvider>   std_status_provider,
             otn::raw::weak_single<const Detail::StdCommandExecutor>  std_command_executor)
        : m_detail{std::move(std_id_provider),
                   std::move(std_structure_scanner),
                   std::move(std_status_provider),
                   std::move(std_command_executor)}
    {}

private:
    otn::raw::weak_single<const IdProvider>       idProvider() const override
    { return m_detail.idProvider(); }
    otn::raw::weak_single<const StructureScanner> structureScanner() const override
    { return m_detail.structureScanner(); }
    otn::raw::weak_single<const StatusProvider>   statusProvider() const override
    { return m_detail.statusProvider(); }
    otn::raw::weak_single<const CommandExecutor>  commandExecutor() const override
    { return m_detail.commandExecutor(); }
    otn::raw::weak_single<const ToolLauncher>     toolLauncher() const override
    { return m_detail.toolLauncher(); }

    void save(Settings::UserStorage& storage) const override
    { return m_detail.save(storage); }
    void load(Settings::UserStorage& storage) override
    { return m_detail.load(storage); }

    Detail m_detail;
};

} // namespace GitComplex::Repository::Service::Qt::Concrete
