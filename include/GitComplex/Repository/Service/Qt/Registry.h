/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Entity/Qt/IdProvider.h>
#include <GitComplex/Repository/Scanner/Entity/Qt/FileSystem.h>
#include <GitComplex/Repository/Tree/Entity/Qt/StatusProvider.h>
#include <GitComplex/Repository/Shell/Entity/Qt/CommandExecutor.h>
#include <GitComplex/Repository/Shell/Entity/Qt/ToolLauncher.h>

#include <otn/all.hpp>

/*!
 * \file include/GitComplex/Repository/Service/Qt/Registry.h
 * \brief include/GitComplex/Repository/Service/Qt/Registry.h
 */

namespace GitComplex::Repository::Service::Qt
{

/*!
 * \class Registry
 * \brief Registry.
 *
 * \headerfile GitComplex/Repository/Service/Qt/Registry.h
 *
 * Registry.
 *
 * \note [[Abstraction::Interface]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class Registry
{
public:
    using IdProvider       = Entity::Qt::IdProvider;
    using StructureScanner = Scanner::Entity::Qt::FileSystem;
    using StatusProvider   = Tree::Entity::Qt::StatusProvider;
    using CommandExecutor  = Shell::Entity::Qt::CommandExecutor;
    using ToolLauncher     = Shell::Entity::Qt::ToolLauncher;

    virtual ~Registry() = default;

    virtual otn::raw::weak_single<const IdProvider>       idProvider() const       = 0;
    virtual otn::raw::weak_single<const StructureScanner> structureScanner() const = 0;
    virtual otn::raw::weak_single<const StatusProvider>   statusProvider() const   = 0;
    virtual otn::raw::weak_single<const CommandExecutor>  commandExecutor() const  = 0;
    virtual otn::raw::weak_single<const ToolLauncher>     toolLauncher() const     = 0;
};

} // namespace GitComplex::Repository::Service::Qt
