/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Entity/Qt/IdProvider.h>
#include <GitComplex/Repository/Scanner/Entity/Qt/FileSystem.h>
#include <GitComplex/Repository/Tree/Entity/Qt/StatusProvider.h>
#include <GitComplex/Repository/Shell/Entity/Qt/CommandExecutor.h>
#include <GitComplex/Repository/Shell/Entity/Qt/ToolLauncher.h>

#include <GitComplex/Repository/Entity/Std/IdProvider.h>
#include <GitComplex/Repository/Scanner/Entity/Std/FileSystem.h>
#include <GitComplex/Repository/Tree/Entity/Std/StatusProvider.h>
#include <GitComplex/Repository/Shell/Entity/Std/CommandExecutor.h>

#include <GitComplex/Settings/Storage.h>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/Service/Qt/Detail/Registry.h
 * \brief include/GitComplex/Repository/Service/Qt/Detail/Registry.h
 */

namespace GitComplex::Repository::Service::Qt::Detail
{

/*!
 * \class Registry
 * \brief Registry.
 *
 * \headerfile GitComplex/Repository/Service/Qt/Detail/Registry.h
 *
 * Registry.
 *
 * \note [[Abstraction::Detail]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        Registry
{
public:
    using IdProvider       = Entity::Qt::IdProvider;
    using StructureScanner = Scanner::Entity::Qt::FileSystem;
    using StatusProvider   = Tree::Entity::Qt::StatusProvider;
    using CommandExecutor  = Shell::Entity::Qt::CommandExecutor;
    using ToolLauncher     = Shell::Entity::Qt::ToolLauncher;

    using StdIdProvider       = Entity::Std::IdProvider;
    using StdStructureScanner = Scanner::Entity::Std::FileSystem;
    using StdStatusProvider   = Tree::Entity::Std::StatusProvider;
    using StdCommandExecutor  = Shell::Entity::Std::CommandExecutor;

    Registry(otn::raw::weak_single<const StdIdProvider>       std_id_provider,
             otn::raw::weak_single<const StdStructureScanner> std_structure_scanner,
             otn::raw::weak_single<const StdStatusProvider>   std_status_provider,
             otn::raw::weak_single<const StdCommandExecutor>  std_command_executor);

    otn::raw::weak_single<const IdProvider>       idProvider() const
    { return m_id_provider; }
    otn::raw::weak_single<const StructureScanner> structureScanner() const
    { return m_structure_scanner; }
    otn::raw::weak_single<const StatusProvider>   statusProvider() const
    { return m_status_provider; }
    otn::raw::weak_single<const CommandExecutor>  commandExecutor() const
    { return m_command_executor; }
    otn::raw::weak_single<const ToolLauncher>     toolLauncher() const
    { return m_tool_launcher; }

    void save(Settings::UserStorage& storage) const;
    void load(Settings::UserStorage& storage);

private:
    otn::slim::unique_single<IdProvider>       m_id_provider;
    otn::slim::unique_single<StructureScanner> m_structure_scanner;
    otn::slim::unique_single<StatusProvider>   m_status_provider;
    otn::slim::unique_single<CommandExecutor>  m_command_executor;
    otn::slim::unique_single<ToolLauncher>     m_tool_launcher;
};

} // namespace GitComplex::Repository::Service::Qt::Detail
