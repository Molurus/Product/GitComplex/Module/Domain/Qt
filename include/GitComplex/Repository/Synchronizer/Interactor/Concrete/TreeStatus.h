/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Synchronizer/Interactor/Concrete/Namespace.h>
#include <GitComplex/Repository/Synchronizer/Entity/TreeStatus.h>
#include <GitComplex/Repository/Synchronizer/Entity/Detail/SelectionStatusUpdater.h>
#include <GitComplex/Repository/Tree/Entity/Qt/Observer.h>
#include <GitComplex/Repository/Tree/Entity/Qt/Detail/StatusUpdater.h>
#include <GitComplex/Repository/Tree/Entity/Qt/Detail/StatusSynchronizer.h>

/*!
 * \file include/GitComplex/Repository/Synchronizer/Interactor/Concrete/TreeStatus.h
 * \brief include/GitComplex/Repository/Synchronizer/Interactor/Concrete/TreeStatus.h
 */

namespace GitComplex::Repository::Synchronizer::Interactor::Concrete
{

/*!
 * \class TreeStatus
 * \brief Concrete tree status synchronizer.
 *
 * \headerfile GitComplex/Repository/Synchronizer/Interactor/Concrete/TreeStatus.h
 *
 * Concrete tree status synchronizer.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class TreeStatus final : public Entity::TreeStatus
{
public:
    TreeStatus(otn::weak_optional<Tree::StatusObserver> status_observer,
               otn::raw::weak_single<const Tree::StatusProvider>  status_provider,
               otn::weak_optional<const Tree::DependenceProvider> dependence_provider,
               const Tree::StructureObservable& structure_observable,
               otn::unique_single<const Selection::Provider> selection_provider)
        : m_tree_updater{otn::itself_type<Tree::Detail::StatusUpdater>,
                         std::move(status_observer),
                         std::move(status_provider),
                         std::move(dependence_provider)},
          m_tree_synchronizer{structure_observable,
                              m_tree_updater},
          m_selection_updater{std::move(selection_provider),
                              m_tree_updater}
    {}

private:
    void updateStatus(Selection::Mode mode) override
    { m_selection_updater.updateRepositoriesStatus(mode); }

    otn::unique_single<Tree::StatusUpdater> m_tree_updater;
    Tree::Detail::StatusSynchronizer m_tree_synchronizer;
    Entity::Detail::SelectionStatusUpdater  m_selection_updater;
};

} // namespace GitComplex::Repository::Synchronizer::Interactor::Concrete
