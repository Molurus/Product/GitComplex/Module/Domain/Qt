/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Synchronizer/Interactor/Concrete/Namespace.h>
#include <GitComplex/Repository/Synchronizer/Entity/TreeStructure.h>
#include <GitComplex/Repository/Synchronizer/Entity/Detail/StructureAdapter.h>
#include <GitComplex/Repository/RootList/Entity/Concrete/Observer.h>
#include <GitComplex/Repository/RootList/Entity/Concrete/Synchronizer.h>

/*!
 * \file include/GitComplex/Repository/Synchronizer/Interactor/Concrete/TreeStructure.h
 * \brief include/GitComplex/Repository/Synchronizer/Interactor/Concrete/TreeStructure.h
 */

namespace GitComplex::Repository::Synchronizer::Interactor::Concrete
{

/*!
 * \class TreeStructure
 * \brief Concrete tree structure synchronizer.
 *
 * \headerfile GitComplex/Repository/Synchronizer/Interactor/Concrete/TreeStructure.h
 *
 * Concrete tree structure synchronizer from a root list.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class TreeStructure final : public Entity::TreeStructure
{
private:
    /*! Detail of the adapter from a root list to a tree structure. */
    using AdapterDetail = Entity::Detail::StructureAdapter;
    /*! Concrete adapter from a root list to a tree structure. */
    using Adapter = RootList::Concrete::Observer<AdapterDetail>;

public:
    using StructureScanner = Scanner::FileSystem;

    TreeStructure(otn::weak_optional<Tree::StructureObserver>     tree_observer,
                  otn::raw::weak_single<const IdProvider>         id_provider,
                  otn::raw::weak_single<const StructureScanner>   structure_scanner,
                  const Repository::RootList::Entity::Observable& root_list_observable,
                  otn::weak_optional<const Repository::RootList::Entity::Provider> root_list_provider)
        : m_adapter{otn::itself_type<Adapter>,
                    AdapterDetail{std::move(tree_observer),
                                  std::move(id_provider),
                                  std::move(structure_scanner)}},
          m_synchronizer{otn::itself_type<RootList::Concrete::Synchronizer>,
                         root_list_observable,
                         std::move(root_list_provider),
                         m_adapter}
    {}

private:
    void synchronizeStructure() const override
    { (*m_synchronizer).synchronize(); }

    /*! Adapter from a root list to a tree structure. */
    otn::unique_single<RootList::Observer>     m_adapter;
    /*! Synchronizer from a root list to the m_adapter. */
    otn::unique_single<RootList::Synchronizer> m_synchronizer;
};

} // namespace GitComplex::Repository::Synchronizer::Interactor::Concrete
