/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Synchronizer/Entity/Detail/Namespace.h>
#include <GitComplex/Repository/Tree/Entity/Qt/StatusUpdater.h>
#include <GitComplex/Repository/Selection/Entity/Provider.h>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/Synchronizer/Entity/Detail/SelectionStatusUpdater.h
 * \brief include/GitComplex/Repository/Synchronizer/Entity/Detail/SelectionStatusUpdater.h
 */

namespace GitComplex::Repository::Synchronizer::Entity::Detail
{

/*!
 * \class SelectionStatusUpdater
 * \brief SelectionStatusUpdater.
 *
 * \headerfile GitComplex/Repository/Synchronizer/Entity/Detail/SelectionStatusUpdater.h
 *
 * SelectionStatusUpdater.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        SelectionStatusUpdater
{
public:
    using SelectionProvider = const Selection::Provider;
    using StatusUpdater     = Tree::StatusUpdater;

    SelectionStatusUpdater(
        otn::unique_single<SelectionProvider> selection_provider,
        otn::weak_optional<StatusUpdater>     updater);

    void updateRepositoriesStatus(Selection::Mode mode);

private:
    otn::unique_single<SelectionProvider> m_selection_provider;
    otn::weak_optional<StatusUpdater>     m_updater;
};

} // namespace GitComplex::Repository::Synchronizer::Entity::Detail
