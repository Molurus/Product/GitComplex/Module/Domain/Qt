/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Synchronizer/Entity/Detail/Namespace.h>
#include <GitComplex/Repository/Tree/Entity/Qt/StructureObserver.h>
#include <GitComplex/Repository/Scanner/Entity/Qt/FileSystem.h>
#include <GitComplex/Repository/Entity/Qt/IdProvider.h>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/Synchronizer/Entity/Detail/StructureAdapter.h
 * \brief include/GitComplex/Repository/Synchronizer/Entity/Detail/StructureAdapter.h
 */

namespace GitComplex::Repository::Synchronizer::Entity::Detail
{

/*!
 * \class StructureAdapter
 * \brief StructureAdapter.
 *
 * \headerfile GitComplex/Repository/Synchronizer/Entity/Detail/StructureAdapter.h
 *
 * StructureAdapter.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        StructureAdapter
{
public:
    using StructureScanner = const Scanner::FileSystem;

    StructureAdapter(otn::weak_optional<Tree::StructureObserver> observer,
                     otn::raw::weak_single<const IdProvider>     id_provider,
                     otn::raw::weak_single<StructureScanner>     scanner);

    void addRepositories(PathList paths);
    void removeRepositories(PathList paths);
    void resetRepositories(PathList paths);

private:
    otn::weak_optional<Tree::StructureObserver> m_observer;
    otn::raw::weak_single<const IdProvider>     m_id_provider;
    otn::raw::weak_single<StructureScanner>     m_scanner;
};

} // namespace GitComplex::Repository::Synchronizer::Entity::Detail
