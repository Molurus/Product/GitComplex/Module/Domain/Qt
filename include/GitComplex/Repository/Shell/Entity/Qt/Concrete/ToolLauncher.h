/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Shell/Entity/Qt/Concrete/Namespace.h>
#include <GitComplex/Repository/Shell/Entity/Qt/ToolLauncher.h>
#include <GitComplex/Repository/Shell/Entity/Qt/Detail/ToolLauncher.h>
#include <GitComplex/Settings/Storable.h>

/*!
 * \file include/GitComplex/Repository/Shell/Entity/Qt/Concrete/ToolLauncher.h
 * \brief include/GitComplex/Repository/Shell/Entity/Qt/Concrete/ToolLauncher.h
 */

namespace GitComplex::Repository::Shell::Entity::Qt::Concrete
{

/*!
 * \struct ToolLauncher
 * \brief ToolLauncher.
 *
 * \headerfile GitComplex/Repository/Shell/Entity/Qt/Concrete/ToolLauncher.h
 *
 * ToolLauncher.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[Framework::Qt]]
 */
class ToolLauncher final : public Qt::ToolLauncher,
                           public Settings::UserStorable
{
public:
    ToolLauncher(otn::raw::weak_single<const CommandExecutor> command_executor)
        : m_detail{std::move(command_executor)}
    {}

private:
    void openTerminal(SingleId id) const override
    { m_detail.openTerminal(std::move(id)); }
    void showModifications(SingleId id) const override
    { m_detail.showModifications(std::move(id)); }
    void showLog(SingleId id) const override
    { m_detail.showLog(std::move(id)); }

    void save(Settings::UserStorage& storage) const override
    { return m_detail.save(storage); }
    void load(Settings::UserStorage& storage) override
    { return m_detail.load(storage); }

    Detail::ToolLauncher m_detail;
};

} // namespace GitComplex::Repository::Shell::Entity::Adapter
