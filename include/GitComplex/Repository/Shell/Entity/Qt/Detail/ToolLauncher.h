/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Shell/Entity/Qt/Detail/Namespace.h>
#include <GitComplex/Repository/Shell/Entity/Qt/CommandExecutor.h>
#include <GitComplex/Settings/Storage.h>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/Shell/Entity/Qt/Detail/ToolLauncher.h
 * \brief include/GitComplex/Repository/Shell/Entity/Qt/Detail/ToolLauncher.h
 */

namespace GitComplex::Repository::Shell::Entity::Qt::Detail
{

/*!
 * \struct ToolLauncher
 * \brief ToolLauncher.
 *
 * \headerfile GitComplex/Repository/Shell/Entity/Qt/Detail/ToolLauncher.h
 *
 * ToolLauncher.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        ToolLauncher
{
public:
    ToolLauncher(otn::raw::weak_single<const CommandExecutor> command_executor);

    void openTerminal(SingleId id) const;
    void showModifications(SingleId id) const;
    void showLog(SingleId id) const;

    void save(Settings::UserStorage& storage) const;
    void load(Settings::UserStorage& storage);

protected:
    void executeCommand(Command::Line command, SingleId id) const;

private:
    otn::raw::weak_single<const CommandExecutor> m_command_executor;
    Command::Line m_open_terminal_command;
    Command::Line m_show_modifications_command;
    Command::Line m_show_log_command;
};

} // namespace GitComplex::Repository::Shell::Entity::Qt::Detail
