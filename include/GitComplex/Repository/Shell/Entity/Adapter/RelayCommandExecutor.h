/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Shell/Entity/Std/CommandExecutor.h>
#include <GitComplex/Repository/Shell/Entity/Qt/CommandExecutor.h>

#include <QObject>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/Shell/Entity/Adapter/RelayCommandExecutor.h
 * \brief include/GitComplex/Repository/Shell/Entity/Adapter/RelayCommandExecutor.h
 */

namespace GitComplex::Repository::Shell::Entity::Adapter
{

namespace Command = ::GitComplex::Shell::Command;

/*!
 * \struct RelayCommandExecutor
 * \brief RelayCommandExecutor.
 *
 * \headerfile GitComplex/Repository/Shell/Entity/Adapter/RelayCommandExecutor.h
 *
 * RelayCommandExecutor.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        RelayCommandExecutor final : public Qt::CommandExecutor
{
public:
    RelayCommandExecutor(otn::raw::weak_single<const Std::CommandExecutor> core_command_executor,
                         QObject& context)
        : m_core_command_executor{std::move(core_command_executor)},
          m_context{context}
    {}

private:
    void executeCommand(
        Command::Qt::Line command,
        Repository::Entity::Qt::IdList ids,
        ReceiveAction<Command::Qt::SummaryList> receive_action) const override;

    static void transmitCommandSummaries(
        ReceiveAction<Command::Qt::SummaryList> receive_action,
        Command::Std::SummaryList command_summaries);

    otn::raw::weak_single<const Std::CommandExecutor> m_core_command_executor;
    QObject& m_context;
};

} // namespace GitComplex::Repository::Shell::Entity::Adapter
