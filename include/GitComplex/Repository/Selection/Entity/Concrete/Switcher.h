/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Selection/Entity/Concrete/Namespace.h>
#include <GitComplex/Repository/Selection/Entity/Switcher.h>
#include <GitComplex/Repository/Selection/Entity/Detail/Switcher.h>
#include <GitComplex/Settings/Storable.h>

/*!
 * \file include/GitComplex/Repository/Selection/Entity/Concrete/Switcher.h
 * \brief include/GitComplex/Repository/Selection/Entity/Concrete/Switcher.h
 */

namespace GitComplex::Repository::Selection::Entity::Concrete
{

/*!
 * \class Switcher
 * \brief Switcher.
 *
 * \headerfile GitComplex/Repository/Selection/Entity/Concrete/Switcher.h
 *
 * Switcher.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[Framework::Qt]]
 */
class Switcher : public Entity::Switcher,
                 public Settings::RepositoryStorable
{
public:
    explicit Switcher(Mode initial_mode)
        : m_detail{initial_mode}
    {}

private:
    void switchToMode(Mode mode) override
    { m_detail.switchToMode(mode); }
    Mode currentMode() const override
    { return m_detail.currentMode(); }

    EventDelegate::Switcher& modeChanged() const override
    { return m_detail.eventDelegate(); }

    void save(Settings::RepositoryStorage& storage) const override
    { return m_detail.save(storage); }

    void load(Settings::RepositoryStorage& storage) override
    { return m_detail.load(storage); }

    Detail::Switcher m_detail;
};

} // namespace GitComplex::Repository::Selection::Entity::Concrete
