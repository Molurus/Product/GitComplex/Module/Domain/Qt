/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Entity/Qt/Namespace.h>
#include <GitComplex/Repository/Entity/Qt/Id.h>
#include <GitComplex/Core/Qt/Path.h>

/*!
 * \file include/GitComplex/Repository/Entity/Qt/IdProvider.h
 * \brief include/GitComplex/Repository/Entity/Qt/IdProvider.h
 */

namespace GitComplex::Repository::Entity::Qt
{

/*!
 * \class IdProvider
 * \brief IdProvider.
 *
 * \headerfile GitComplex/Repository/Entity/Qt/IdProvider.h
 *
 * IdProvider.
 *
 * \note [[Abstraction::Interface]] [[Viper::Entity]] [[Framework::Qt]]
 */
class IdProvider
{
public:
    virtual ~IdProvider() = default;

    virtual SingleId id(const Path& path) const       = 0;
    virtual IdList   ids(const PathList& paths) const = 0;

    virtual Path     path(const SingleId& id) const = 0;
};

} // namespace GitComplex::Repository::Entity::Qt
