/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Entity/Std/IdProvider.h>
#include <GitComplex/Repository/Entity/Std/Detail/IdProviderUser.h>
#include <GitComplex/Repository/Entity/Qt/IdProvider.h>
#include <GitComplex/Core/Converter/Path.h>

/*!
 * \file include/GitComplex/Repository/Entity/Adapter/BlockingIdProvider.h
 * \brief include/GitComplex/Repository/Entity/Adapter/BlockingIdProvider.h
 */

namespace GitComplex::Repository::Entity::Adapter
{

/*!
 * \class BlockingIdProvider
 * \brief BlockingIdProvider.
 *
 * \headerfile GitComplex/Repository/Entity/Adapter/BlockingIdProvider.h
 *
 * BlockingIdProvider.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
class BlockingIdProvider final : public Qt::IdProvider,
                                 private Std::Detail::IdProviderUser
{
public:
    explicit BlockingIdProvider(
        otn::raw::weak_single<const Std::IdProvider> id_provider)
        : IdProviderUser{std::move(id_provider)}
    {}

private:
    Qt::SingleId id(const Qt::Path& path) const override
    { return idProvider().id(Converter::to<Std::Path>(path)); }
    Qt::IdList   ids(const Qt::PathList& paths) const override
    { return idProvider().ids(Converter::to<Std::PathList>(paths)); }

    Qt::Path path(const Qt::SingleId& id) const override
    { return Converter::to<Qt::Path>(idProvider().path(id)); }
};

} // namespace GitComplex::Repository::Entity::Adapter
