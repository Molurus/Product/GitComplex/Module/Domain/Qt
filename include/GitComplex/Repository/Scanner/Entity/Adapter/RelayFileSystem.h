/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Scanner/Entity/Std/FileSystem.h>
#include <GitComplex/Repository/Scanner/Entity/Qt/FileSystem.h>

#include <QObject>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/Scanner/Entity/Adapter/RelayFileSystem.h
 * \brief include/GitComplex/Repository/Scanner/Entity/Adapter/RelayFileSystem.h
 */

namespace GitComplex::Repository::Scanner::Entity::Adapter
{

/*!
 * \struct RelayFileSystem
 * \brief RelayFileSystem.
 *
 * \headerfile GitComplex/Repository/Scanner/Entity/Adapter/RelayFileSystem.h
 *
 * RelayFileSystem.
 *
 * \note [[using Abstraction : Detail, Concrete]]
 *       [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        RelayFileSystem final : public Qt::FileSystem
{
public:
    RelayFileSystem(
        otn::raw::weak_single<const Std::FileSystem> std_structure_scanner,
        QObject& context)
        : m_std_structure_scanner{std::move(std_structure_scanner)},
          m_context{context}
    {}

private:
    using IdList = Repository::Entity::Qt::IdList;

    void scan(
        Core::Qt::PathList paths,
        ReceiveAction<Qt::Structure::List> receive_action) const override;

    // Transmit methods.
    static void transmitStructures(
        ReceiveAction<Qt::Structure::List> receive_action,
        Std::Structure::List structures);

    otn::raw::weak_single<const Std::FileSystem> m_std_structure_scanner;
    QObject& m_context;
};

} // namespace GitComplex::Repository::Scanner::Entity::Adapter
