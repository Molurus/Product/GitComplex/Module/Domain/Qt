/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/RootList/Entity/EventDelegate/RootListObserver.h>
#include <GitComplex/Repository/RootList/Entity/EventDelegate/RootListProvider.h>
#include <GitComplex/Settings/Storage.h>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/RootList/Entity/Detail/Storage.h
 * \brief include/GitComplex/Repository/RootList/Entity/Detail/Storage.h
 */

namespace GitComplex::Repository::RootList::Entity::Detail
{

/*!
 * \class Storage
 * \brief Storage.
 *
 * \headerfile GitComplex/Repository/RootList/Entity/Detail/Storage.h
 *
 * Storage.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        Storage
{
public:
    Storage();

    void save(Settings::RepositoryStorage& storage) const;
    void load(Settings::RepositoryStorage& storage);

    void addRepositories(const PathList& paths);
    void removeRepositories(const PathList& paths);
    void resetRepositories(const PathList& paths);

    const PathList& repositories() const { return m_repositories; }

    EventDelegate::Observer& observerEventDelegate() const
    { return *m_observer_event_delegate; }

    EventDelegate::Provider& providerEventDelegate() const
    { return *m_provider_event_delegate; }

protected:
    bool addRepository(const Path& path);
    bool removeRepository(const Path& path);

private:
    PathList m_repositories;
    otn::raw::unique_single<EventDelegate::Observer> m_observer_event_delegate;
    otn::raw::unique_single<EventDelegate::Provider> m_provider_event_delegate;
};

} // namespace GitComplex::Repository::RootList::Entity::Detail
