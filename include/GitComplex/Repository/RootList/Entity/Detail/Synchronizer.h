/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/RootList/Entity/Storage.h>
#include <GitComplex/Core/Qt/ObjectConnection.h>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/RootList/Entity/Detail/Synchronizer.h
 * \brief include/GitComplex/Repository/RootList/Entity/Detail/Synchronizer.h
 */

namespace GitComplex::Repository::RootList::Entity::Detail
{

/*!
 * \class Synchronizer
 * \brief Synchronizer.
 *
 * \headerfile GitComplex/Repository/RootList/Entity/Detail/Synchronizer.h
 *
 * Synchronizer.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        Synchronizer
{
public:
    Synchronizer(const Observable& observable,
                 otn::weak_optional<const Provider> provider,
                 otn::weak_optional<Observer>       observer);

    void synchronize() const;

private:
    otn::weak_optional<const Provider> m_provider;
    otn::weak_optional<Observer>       m_observer;
    FixedObjectConnections<2> m_connections;
};

} // namespace GitComplex::Repository::RootList::Entity::Detail
