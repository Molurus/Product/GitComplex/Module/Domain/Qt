/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/RootList/Entity/Observer.h>

/*!
 * \file include/GitComplex/Repository/RootList/Entity/Concrete/Observer.h
 * \brief include/GitComplex/Repository/RootList/Entity/Concrete/Observer.h
 */

namespace GitComplex::Repository::RootList::Entity::Concrete
{

/*!
 * \class Observer
 * \brief Observer.
 *
 * \headerfile GitComplex/Repository/RootList/Entity/Concrete/Observer.h
 *
 * Observer.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Detail>
class Observer final : public Entity::Observer
{
public:
    explicit Observer(Detail detail)
        : m_detail{std::move(detail)}
    {}

private:
    void addRepositories(PathList paths) override
    { m_detail.addRepositories(std::move(paths)); }
    void removeRepositories(PathList paths) override
    { m_detail.removeRepositories(std::move(paths)); }
    void resetRepositories(PathList paths) override
    { m_detail.resetRepositories(std::move(paths)); }

    Detail m_detail;
};

} // namespace GitComplex::Repository::RootList::Entity::Concrete
