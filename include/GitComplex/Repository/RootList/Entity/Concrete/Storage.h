/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/RootList/Entity/Storage.h>
#include <GitComplex/Repository/RootList/Entity/Detail/Storage.h>
#include <GitComplex/Settings/Storable.h>

/*!
 * \file include/GitComplex/Repository/RootList/Entity/Concrete/Storage.h
 * \brief include/GitComplex/Repository/RootList/Entity/Concrete/Storage.h
 */

namespace GitComplex::Repository::RootList::Entity::Concrete
{

/*!
 * \class Storage
 * \brief Storage.
 *
 * \headerfile GitComplex/Repository/RootList/Entity/Concrete/Storage.h
 *
 * Storage.
 *
 * \note [[Abstraction::Concrete]] [[Viper::Entity]] [[Framework::Qt]]
 */
class Storage final : public Entity::Storage,
                      public Settings::RepositoryStorable
{
public:
    Storage() = default;

private:
    PathList repositories() const override
    { return m_detail.repositories(); }

    EventDelegate::Provider& hasRepositoriesChanged() const override
    { return m_detail.providerEventDelegate(); }

    void addRepositories(PathList paths) override
    { m_detail.addRepositories(paths); }
    void removeRepositories(PathList paths) override
    { m_detail.removeRepositories(paths); }
    void resetRepositories(PathList paths) override
    { m_detail.resetRepositories(paths); }

    EventDelegate::Observer& repositoriesAdded() const override
    { return m_detail.observerEventDelegate(); }
    EventDelegate::Observer& repositoriesRemoved() const override
    { return m_detail.observerEventDelegate(); }

    void save(Settings::RepositoryStorage& storage) const override
    { return m_detail.save(storage); }

    void load(Settings::RepositoryStorage& storage) override
    { return m_detail.load(storage); }

    Detail::Storage m_detail;
};

} // namespace GitComplex::Repository::RootList::Entity::Concrete
