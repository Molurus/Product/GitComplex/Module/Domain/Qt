/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/RootList/Interactor/Detail/Namespace.h>
#include <GitComplex/Repository/RootList/Entity/Observer.h>
#include <GitComplex/Repository/RootList/Entity/Provider.h>

#include <otn/all.hpp>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/Repository/RootList/Interactor/Detail/Editor.h
 * \brief include/GitComplex/Repository/RootList/Interactor/Detail/Editor.h
 */

namespace GitComplex::Repository::RootList::Interactor::Detail
{

/*!
 * \class Editor
 * \brief Editor.
 *
 * \headerfile GitComplex/Repository/RootList/Interactor/Detail/Editor.h
 *
 * Editor.
 *
 * \note [[Abstraction::Detail]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        Editor
{
public:
    using ProviderEventDelegate = Provider::ProviderEventDelegate;

    Editor(otn::weak_optional<Observer>    observer,
           otn::raw::weak_single<Provider> add_provider,
           otn::raw::weak_single<Provider> remove_provider);

    void addRepositories();
    void removeRepositories();

    ProviderEventDelegate& hasRepositoriesToRemoveChanged() const
    { return (*m_remove_provider).hasRepositoriesChanged(); }

private:
    otn::weak_optional<Observer>    m_observer;
    otn::raw::weak_single<Provider> m_add_provider;
    otn::raw::weak_single<Provider> m_remove_provider;
};

} // namespace GitComplex::Repository::RootList::Interactor::Detail
