/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Settings/Storable.h>
#include <GitComplex/Settings/Trait.h>

/*!
 * \file include/GitComplex/Settings/Utility.h
 * \brief include/GitComplex/Settings/Utility.h
 */

namespace GitComplex::Settings
{

/*!
 * save_pmr.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class Storage>
inline
void save_pmr(const Object& object, Storage&& storage)
{
    using Storable = Settings::Storable<Trait::Scope<Storage>>;
    if (const auto* storable =
            dynamic_cast<const Storable*>(std::addressof(object)))
        storable->save(storage);
}

/*!
 * save_pmr.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class Storage>
inline
void save_pmr(const Object& object, Storage&& storage, const QString& group)
{
    using Storable = Settings::Storable<Trait::Scope<Storage>>;
    if (const auto* storable =
            dynamic_cast<const Storable*>(std::addressof(object)))
    {
        storage.beginGroup(group);
        storable->save(storage);
        storage.endGroup();
    }
}

/*!
 * load_pmr.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class Storage>
inline
void load_pmr(Object& object, Storage&& storage)
{
    using Storable = Settings::Storable<Trait::Scope<Storage>>;
    if (auto* storable =
            dynamic_cast<Storable*>(std::addressof(object)))
        storable->load(storage);
}

/*!
 * load_pmr.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class Storage>
inline
void load_pmr(Object& object, Storage&& storage, const QString& group)
{
    using Storable = Settings::Storable<Trait::Scope<Storage>>;
    if (auto* storable =
            dynamic_cast<Storable*>(std::addressof(object)))
    {
        storage.beginGroup(group);
        storable->load(storage);
        storage.endGroup();
    }
}

/*!
 * save_cnt.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class Storage>
inline
void save_cnt(const Object& object, Storage&& storage)
{
    object.save(storage);
}

/*!
 * save_cnt.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class Storage>
inline
void save_cnt(const Object& object, Storage&& storage, const QString& group)
{
    storage.beginGroup(group);
    save_cnt(object, storage);
    storage.endGroup();
}

/*!
 * load_cnt.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class Storage>
inline
void load_cnt(Object& object, Storage&& storage)
{
    object.load(storage);
}

/*!
 * load_cnt.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class Storage>
inline
void load_cnt(Object& object, Storage&& storage, const QString& group)
{
    storage.beginGroup(group);
    load_cnt(object, storage);
    storage.endGroup();
}

} // namespace GitComplex::Settings
