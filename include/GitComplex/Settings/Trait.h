/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Settings/Scope.h>

#include <type_traits>

/*!
 * \file include/GitComplex/Settings/Trait.h
 * \brief include/GitComplex/Settings/Trait.h
 */

namespace GitComplex::Settings
{

namespace Trait
{

namespace Internal
{

template <class Subject>
struct Scope
{
    static constexpr Settings::Scope Value =
        Scope<std::decay_t<Subject>>::Value;
};

template <template <Settings::Scope ScopeType> class Subject,
          Settings::Scope ScopeType>
struct Scope<Subject<ScopeType>>
{ static constexpr Settings::Scope Value = ScopeType; };

} // namespace Internal

/*!
 * Scope.
 *
 * \note [[Abstraction::DataType]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Subject>
inline constexpr Settings::Scope Scope = Internal::Scope<Subject>::Value;

} // namespace Trait

} // namespace GitComplex::Settings
