/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <vector>
#include <QStringList>

#include <GitComplex/Core/Converter/String.h>

/*!
 * \file include/GitComplex/Core/Converter/StringList.h
 * \brief include/GitComplex/Core/Converter/StringList.h
 */

namespace GitComplex::Converter
{

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
inline
auto convert(const std::vector<std::string>& strings, To<QStringList> = {})
{
    QStringList to_strings;

    for (const auto& str:strings)
        to_strings.append(to<QString>(str));

    return to_strings;
}

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
inline
auto convert(const QStringList& strings, To<std::vector<std::string>> = {})
{
    std::vector<std::string> to_strings;

    for (const auto& str:strings)
        to_strings.push_back(to<std::string>(str));

    return to_strings;
}

} // namespace GitComplex::Converter
