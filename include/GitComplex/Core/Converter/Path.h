/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Core/Std/Path.h>
#include <GitComplex/Core/Qt/Path.h>

#include <GitComplex/Converter/To.h>

/*!
 * \file include/GitComplex/Core/Converter/Path.h
 * \brief include/GitComplex/Core/Converter/Path.h
 */

namespace GitComplex::Converter
{

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
inline
auto convert(const Core::Std::Path& path, To<Core::Qt::Path> = {})
{ return Core::Qt::Path{QString::fromStdString(path.string())}; }

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
inline
auto convert(const Core::Std::PathList& paths, To<Core::Qt::PathList> = {})
{
    Core::Qt::PathList to_paths;

    for (const auto& path : paths)
        to_paths.append(to<Core::Qt::Path>(path));

    return to_paths;
}

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
inline
auto convert(const Core::Qt::Path& path, To<Core::Std::Path> = {})
{ return Core::Std::Path{path.toStdString()}; }

/*!
 * convert.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[using Framework : Qt, Std]]
 */
inline
auto convert(const Core::Qt::PathList& paths, To<Core::Std::PathList> = {})
{
    Core::Std::PathList to_paths;

    for (const auto& path : paths)
        to_paths.push_back(to<Core::Std::Path>(path));

    return to_paths;
}

} // namespace GitComplex::Converter
