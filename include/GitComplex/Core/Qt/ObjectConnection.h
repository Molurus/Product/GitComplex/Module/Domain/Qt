/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <QObject>
#include <QMetaObject>

#include <array>

/*!
 * \file include/GitComplex/Core/Qt/ObjectConnection.h
 * \brief include/GitComplex/Core/Qt/ObjectConnection.h
 */

namespace GitComplex::Core::Qt
{

/*!
 * \class ObjectConnection
 * \brief ObjectConnection.
 *
 * \headerfile GitComplex/Core/Qt/ObjectConnection.h
 *
 * ObjectConnection.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
class ObjectConnection : public QMetaObject::Connection
{
public:
    ObjectConnection(const ObjectConnection& other) = delete;
    ObjectConnection(ObjectConnection&& other)      = default;

    ObjectConnection(const QMetaObject::Connection& connection) = delete;
    ObjectConnection(QMetaObject::Connection&& connection)
        : QMetaObject::Connection{std::move(connection)}
    {}

    ~ObjectConnection() { QObject::disconnect(*this); }
};

/*!
 * FixedObjectConnections.
 *
 * \note [[Abstraction::Alias]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <std::size_t Count>
using FixedObjectConnections = std::array<ObjectConnection, Count>;

/*!
 * DynamicObjectConnections.
 *
 * \note [[Abstraction::Alias]] [[Viper::Entity]] [[Framework::Qt]]
 */
using DynamicObjectConnections = std::vector<ObjectConnection>;

} // namespace GitComplex::Core::Qt
