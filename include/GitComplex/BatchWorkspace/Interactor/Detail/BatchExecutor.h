/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Shell/Entity/Qt/CommandExecutor.h>
#include <GitComplex/Repository/Selection/Entity/Provider.h>
#include <GitComplex/Console/Entity/Array.h>

#include <otn/all.hpp>

/*!
 * \file include/GitComplex/BatchWorkspace/Interactor/Detail/BatchExecutor.h
 * \brief include/GitComplex/BatchWorkspace/Interactor/Detail/BatchExecutor.h
 */

namespace GitComplex::BatchWorkspace::Interactor::Detail
{

namespace Command = Shell::Command::Qt;

/*!
 * \class BatchExecutor
 * \brief BatchExecutor.
 *
 * \headerfile GitComplex/BatchWorkspace/Interactor/Detail/BatchExecutor.h
 *
 * BatchExecutor.
 *
 * \note [[Abstraction::Detail]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class BatchExecutor
{
public:
    using IdList            = Repository::Entity::Qt::IdList;
    using ListExecutor      = const Repository::Shell::Entity::Qt::CommandExecutor;
    using ConsoleArray      = Console::Entity::Array;
    using SelectionProvider = const Repository::Selection::Entity::Provider;
    using SelectionMode     = Repository::Selection::Entity::Mode;

    BatchExecutor(otn::raw::weak_single<const ListExecutor> list_executor,
                  otn::weak_optional<ConsoleArray>      console_array,
                  otn::weak_optional<SelectionProvider> selection_provider);

    void executeCommand(SelectionMode mode) const;
    void executeCommand(SelectionMode mode, Command::Line command) const;
    void executeCommand(SelectionMode mode, Command::Line command,
                        IdList ids) const;

private:
    Command::Line command(SelectionMode mode) const;
    IdList repositories(SelectionMode mode) const;

    otn::raw::weak_single<const ListExecutor> m_list_executor;
    otn::weak_optional<ConsoleArray> m_console_array;
    otn::weak_optional<SelectionProvider>     m_selection_provider;
};

} // namespace GitComplex::BatchWorkspace::Interactor::Detail
