/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Repository/Selection/Entity/Switcher.h>
#include <GitComplex/Repository/Shell/Entity/Qt/CommandExecutor.h>
#include <GitComplex/BatchWorkspace/Interactor/Detail/BatchExecutor.h>
#include <GitComplex/Settings/Storage.h>

#include <GitComplex/Domain/Qt/Export.h>

/*!
 * \file include/GitComplex/BatchWorkspace/Interactor/Detail/BatchConsole.h
 * \brief include/GitComplex/BatchWorkspace/Interactor/Detail/BatchConsole.h
 */

namespace GitComplex::BatchWorkspace::Interactor::Detail
{

/*!
 * \class BatchConsole
 * \brief BatchConsole.
 *
 * \headerfile GitComplex/BatchWorkspace/Interactor/BatchConsole.h
 *
 * BatchConsole.
 *
 * \note [[Abstraction::Detail]] [[Viper::Interactor]] [[Framework::Qt]]
 */
class GITCOMPLEX_DOMAIN_QT_EXPORT
        BatchConsole
{
public:
    using ListExecutor      = BatchExecutor::ListExecutor;
    using ConsoleArray      = BatchExecutor::ConsoleArray;
    using SelectionProvider = BatchExecutor::SelectionProvider;

    using SelectionSwitcher = Repository::Selection::Entity::Switcher;
    using SelectionMode     = Repository::Selection::Entity::Mode;

    BatchConsole(otn::raw::weak_single<ListExecutor>   list_executor,
                 otn::unique_single<ConsoleArray>      console_array,
                 otn::unique_single<SelectionProvider> selection_provider,
                 otn::unique_single<SelectionSwitcher> selection_switcher);

    void save(Settings::RepositoryStorage& storage) const;
    void load(Settings::RepositoryStorage& storage);

    void switchToMode(SelectionMode mode)
    { (*m_selection_switcher).switchToMode(mode); }

    SelectionMode currentMode() const
    { return (*m_selection_switcher).currentMode(); }

    SelectionSwitcher::SwitcherEventDelegate& modeChanged() const
    { return (*m_selection_switcher).modeChanged(); }

    void clearCurrentOutput() const;

private:
    otn::unique_single<ConsoleArray>      m_console_array;
    otn::unique_single<SelectionProvider> m_selection_provider;
    otn::unique_single<SelectionSwitcher> m_selection_switcher;
    otn::unique_single<BatchExecutor>     m_batch_executor;
};

} // namespace GitComplex::BatchWorkspace::Interactor::Detail
