/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <GitComplex/Utility/ActionBuilder.h>

#include <QMetaObject>

/*!
 * \file include/GitComplex/Utility/Qt/ActionBuilder.h
 * \brief include/GitComplex/Utility/Qt/ActionBuilder.h
 */

namespace GitComplex::Utility::Qt
{

/*!
 * makeRelayAction.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class ReturnValue, class ... Args>
inline
auto makeRelayAction(Object* receiver, ReturnValue (Object::* method)(Args ...))
{
    return [ = ](Args ... args)
           {
               // TODO: Remove the otn::unique_carrier when the QTBUG-69683 is fixed.
               QMetaObject::invokeMethod(receiver,
                                         [ =, fwd_args = std::make_tuple(receiver,
                                                                         (otn::unique_carrier{std::forward<Args>(args)}, ...))]() mutable
                                         { std::apply(method, std::move(fwd_args)); });
           };
}

/*!
 * makeRelayAction.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class ReturnValue, class ... Args>
inline
auto makeRelayAction(QObject* context, Object* receiver, ReturnValue (Object::* method)(Args ...))
{
    return [ = ](Args ... args)
           {
               // TODO: Remove the otn::unique_carrier when the QTBUG-69683 is fixed.
               QMetaObject::invokeMethod(context,
                                         [ =, fwd_args = std::make_tuple(receiver,
                                                                         (otn::unique_carrier{std::forward<Args>(args)}, ...))]() mutable
                                         { std::apply(method, std::move(fwd_args)); });
           };
}

/*!
 * makeRelayAction.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class ReturnValue, class Action, class ... Args>
inline
auto makeRelayAction(QObject* context, Object* receiver, ReturnValue (Object::* method)(Action, Args ...), Action action)
{
    return [ =, action = std::forward<Action>(action)](Args ... args) mutable
           {
               // TODO: Remove the otn::unique_carrier when the QTBUG-69683 is fixed.
               QMetaObject::invokeMethod(context,
                                         [ =, fwd_args = std::make_tuple(receiver,
                                                                         std::forward<Action>(action),
                                                                         (otn::unique_carrier{std::forward<Args>(args)}, ...))]() mutable
                                         { std::apply(method, std::move(fwd_args)); });
           };
}

/*!
 * makeRelayAction.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class Object, class ReturnValue, class Action, class ... Args>
inline
auto makeRelayAction(QObject* context, const Object* receiver, ReturnValue (Object::* method)(Action, Args ...) const, Action action)
{
    return [ =, action = std::forward<Action>(action)](Args ... args) mutable
           {
               // TODO: Remove the otn::unique_carrier when the QTBUG-69683 is fixed.
               QMetaObject::invokeMethod(context,
                                         [ =, fwd_args = std::make_tuple(receiver,
                                                                         std::forward<Action>(action),
                                                                         (otn::unique_carrier{std::forward<Args>(args)}, ...))]() mutable
                                         { std::apply(method, std::move(fwd_args)); });
           };
}

/*!
 * makeRelayAction.
 *
 * \note [[Abstraction::Detail]] [[Viper::Entity]] [[Framework::Qt]]
 */
template <class ReturnValue, class Action, class ... Args>
inline
auto makeRelayAction(QObject* context, ReturnValue (* function)(Action, Args ...), Action action)
{
    return [ =, action = std::forward<Action>(action)](Args ... args) mutable
           {
               // TODO: Remove the otn::unique_carrier when the QTBUG-69683 is fixed.
               QMetaObject::invokeMethod(context,
                                         [ =, fwd_args = std::make_tuple(std::forward<Action>(action),
                                                                         (otn::unique_carrier{std::forward<Args>(args)}, ...))]() mutable
                                         { std::apply(function, std::move(fwd_args)); });
           };
}

} // namespace GitComplex::Utility::Qt
