/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/BatchWorkspace/Interactor/Detail/BatchConsole.h>
#include <GitComplex/BatchWorkspace/Interactor/Detail/Namespace.h>
#include <GitComplex/Settings/Utility.h>

/*!
 * \file src/GitComplex/BatchWorkspace/Interactor/Detail/BatchConsole.cpp
 * \brief src/GitComplex/BatchWorkspace/Interactor/Detail/BatchConsole.cpp
 */

namespace GitComplex::BatchWorkspace::Interactor::Detail
{

namespace
{

void connect(const BatchConsole::SelectionProvider& selection_provider,
             const otn::weak_optional<BatchConsole::ConsoleArray>& console_array)
{
    using EventDelegate = BatchConsole::SelectionProvider::ProviderEventDelegate;
    using SelectionMode = BatchConsole::SelectionMode;

    QObject::connect(&selection_provider.hasRepositoriesChanged(),
                     &EventDelegate::hasRepositoriesChanged,
                     [ = ](SelectionMode mode, bool has_repositories)
            {
                otn::access(console_array, [&](auto& console_array)
                {
                    console_array.adjustHasTarget(
                        static_cast<std::size_t>(mode), has_repositories);
                });
            });
}

void connect(const BatchConsole::ConsoleArray& console_array,
             const otn::weak_optional<Detail::BatchExecutor>& batch_executor)
{
    using EventDelegate = BatchConsole::ConsoleArray::ArrayEventDelegate;
    using SelectionMode = BatchConsole::SelectionMode;

    QObject::connect(&console_array.commandExecutionRequested(),
                     &EventDelegate::commandExecutionRequested,
                     [ = ](std::size_t console_index,
                           const Command::Line& command)
            {
                otn::access(batch_executor, [&](auto& batch_executor)
                {
                    batch_executor.executeCommand(
                        static_cast<SelectionMode>(console_index), command);
                });
            });
}

} // namespace

BatchConsole::BatchConsole(
    otn::raw::weak_single<ListExecutor>   list_executor,
    otn::unique_single<ConsoleArray>      console_array,
    otn::unique_single<SelectionProvider> selection_provider,
    otn::unique_single<SelectionSwitcher> selection_switcher)
    : m_console_array{std::move(console_array)},
      m_selection_provider{std::move(selection_provider)},
      m_selection_switcher{std::move(selection_switcher)},
      m_batch_executor{otn::itself,
                       std::move(list_executor),
                       m_console_array,
                       m_selection_provider}
{
    connect(*m_selection_provider, m_console_array);
    connect(*m_console_array,      m_batch_executor);
}

void BatchConsole::save(Settings::RepositoryStorage& storage) const
{
    Settings::save_pmr(*m_console_array,      storage, "ConsoleArray");
    Settings::save_pmr(*m_selection_switcher, storage, "Switcher");
}

void BatchConsole::load(Settings::RepositoryStorage& storage)
{
    Settings::load_pmr(*m_console_array,      storage, "ConsoleArray");
    Settings::load_pmr(*m_selection_switcher, storage, "Switcher");
}

void BatchConsole::clearCurrentOutput() const
{
    (*m_console_array).clearOutput(
        static_cast<std::size_t>((*m_selection_switcher).currentMode()));
}

} // namespace GitComplex::BatchWorkspace::Interactor::Detail
