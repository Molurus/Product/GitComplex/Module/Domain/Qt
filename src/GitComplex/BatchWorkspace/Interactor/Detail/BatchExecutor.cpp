/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/BatchWorkspace/Interactor/Detail/BatchExecutor.h>

/*!
 * \file src/GitComplex/BatchWorkspace/Interactor/Detail/BatchExecutor.cpp
 * \brief src/GitComplex/BatchWorkspace/Interactor/Detail/BatchExecutor.cpp
 */

namespace GitComplex::BatchWorkspace::Interactor::Detail
{

BatchExecutor::BatchExecutor(
    otn::raw::weak_single<const ListExecutor> list_executor,
    otn::weak_optional<ConsoleArray>      console_array,
    otn::weak_optional<SelectionProvider> selection_provider)
    : m_list_executor{std::move(list_executor)},
      m_console_array{std::move(console_array)},
      m_selection_provider{std::move(selection_provider)}
{}

void BatchExecutor::executeCommand(SelectionMode mode) const
{
    executeCommand(mode, command(mode));
}

void BatchExecutor::executeCommand(SelectionMode mode,
                                   Command::Line command) const
{
    if (!command.isEmpty())
        executeCommand(mode, std::move(command), repositories(mode));
}

void BatchExecutor::executeCommand(SelectionMode mode,
                                   Command::Line command,
                                   IdList        ids) const
{
    if (command.isEmpty() || ids.empty())
        return;

    (*m_list_executor).executeCommand(
        std::move(command),
        std::move(ids),
        [ = ](const Command::SummaryList& summaries)
        {
            if (auto console_array = otn::gain(m_console_array))
                (*console_array).printCommandSummaries(
                    static_cast<std::size_t>(mode), summaries);
        });
}

Command::Line BatchExecutor::command(SelectionMode mode) const
{
    if (auto console_array = otn::gain(m_console_array))
        return (*console_array).currentCommand(static_cast<std::size_t>(mode));

    return {};
}

BatchExecutor::IdList BatchExecutor::repositories(SelectionMode mode) const
{
    if (auto selection_provider = otn::gain(m_selection_provider))
        return (*selection_provider).repositories(mode);

    return {};
}

} // namespace GitComplex::BatchWorkspace::Interactor::Detail
