/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Shell/Entity/Adapter/RelayCommandExecutor.h>
#include <GitComplex/Shell/Command/Converter/Summary.h>
#include <GitComplex/Core/Qt/ActionBuilder.h>

/*!
 * \file src/GitComplex/Repository/Shell/Entity/Adapter/RelayCommandExecutor.cpp
 * \brief src/GitComplex/Repository/Shell/Entity/Adapter/RelayCommandExecutor.cpp
 */

namespace GitComplex::Repository::Shell::Entity::Adapter
{

void RelayCommandExecutor::executeCommand(
    Command::Qt::Line command,
    Repository::Entity::Qt::IdList ids,
    ReceiveAction<Command::Qt::SummaryList> receive_action) const
{
    (*m_core_command_executor).executeCommand(
        Converter::to<Command::Std::Line>(command),
        std::move(ids),
        Core::Qt::makeRelayAction(&m_context,
                                  &RelayCommandExecutor::transmitCommandSummaries,
                                  std::move(receive_action)));
}

void RelayCommandExecutor::transmitCommandSummaries(
    ReceiveAction<Command::Qt::SummaryList> receive_action,
    Command::Std::SummaryList command_summaries)
{
    using Command::Qt::SummaryList;
    receive_action(Converter::to<SummaryList>(std::move(command_summaries)));
}

} // namespace GitComplex::Repository::Shell::Entity::Adapter
