/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Shell/Entity/Qt/Detail/ToolLauncher.h>

/*!
 * \file src/GitComplex/Repository/Shell/Entity/Qt/Detail/ToolLauncher.cpp
 * \brief src/GitComplex/Repository/Shell/Entity/Qt/Detail/ToolLauncher.cpp
 */

namespace GitComplex::Repository::Shell::Entity::Qt::Detail
{

namespace
{

#ifdef _WIN32
const QString OpenTerminalCommand{"cmd /c start"};
#else
const QString OpenTerminalCommand{"xterm"};
#endif

const QString ShowModificationsCommand{"git gui"};
const QString ShowLogCommand{"gitk"};

} // namespace

ToolLauncher::ToolLauncher(
    otn::raw::weak_single<const CommandExecutor> command_executor)
    : m_command_executor{std::move(command_executor)},
      m_open_terminal_command{OpenTerminalCommand},
      m_show_modifications_command{ShowModificationsCommand},
      m_show_log_command{ShowLogCommand}
{}

void ToolLauncher::openTerminal(SingleId id) const
{
    executeCommand(m_open_terminal_command, std::move(id));
}

void ToolLauncher::showModifications(SingleId id) const
{
    executeCommand(m_show_modifications_command, std::move(id));
}

void ToolLauncher::showLog(SingleId id) const
{
    executeCommand(m_show_log_command, std::move(id));
}

void ToolLauncher::save(Settings::UserStorage& storage) const
{
    storage.beginGroup("Command");
    storage.setValue("OpenTerminal",      m_open_terminal_command);
    storage.setValue("ShowModifications", m_show_modifications_command);
    storage.setValue("ShowLog",           m_show_log_command);
    storage.endGroup();
}

void ToolLauncher::load(Settings::UserStorage& storage)
{
    storage.beginGroup("Command");
    m_open_terminal_command =
        storage.value("OpenTerminal", OpenTerminalCommand).toString();
    m_show_modifications_command =
        storage.value("ShowModifications", ShowModificationsCommand).toString();
    m_show_log_command =
        storage.value("ShowLog", ShowLogCommand).toString();
    storage.endGroup();
}

void ToolLauncher::executeCommand(Command::Line command, SingleId id) const
{
    (*m_command_executor).executeCommand(
        std::move(command),
        IdList{std::move(id)},
        [ = ](Command::SummaryList) {});
}

} // namespace GitComplex::Repository::Shell::Entity::Qt::Detail
