/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Tree/Entity/Qt/Detail/StatusUpdater.h>
#include <GitComplex/Core/ActionBuilder.h>

/*!
 * \file src/GitComplex/Repository/Tree/Entity/Qt/Detail/StatusUpdater.cpp
 * \brief src/GitComplex/Repository/Tree/Entity/Qt/Detail/StatusUpdater.cpp
 */

namespace GitComplex::Repository::Tree::Entity::Qt::Detail
{

StatusUpdater::StatusUpdater(
    otn::weak_optional<StatusObserver> observer,
    otn::raw::weak_single<const StatusProvider>  status_provider,
    otn::weak_optional<const DependenceProvider> dependence_provider)
    : m_observer{std::move(observer)},
      m_status_provider{std::move(status_provider)},
      m_dependence_provider{std::move(dependence_provider)}
{}

void StatusUpdater::updateSuperprojectsStatus(IdList ids)
{
    if (ids.empty())
        return;

    if (auto dependence_provider = otn::gain(m_dependence_provider))
        (*m_status_provider).requestStatus(
            (*dependence_provider).dependentSuperprojects(ids),
            makeDirectAction(m_observer,
                             &StatusObserver::updateRepositoriesStatus));
}

void StatusUpdater::updateSubmodulesStatus(IdList ids)
{
    if (ids.empty())
        return;

    if (auto dependence_provider = otn::gain(m_dependence_provider))
        (*m_status_provider).requestStatus(
            (*dependence_provider).dependentSubmodules(ids),
            makeDirectAction(m_observer,
                             &StatusObserver::updateRepositoriesStatus));
}

} // namespace GitComplex::Repository::Tree::Entity::Qt::Detail
