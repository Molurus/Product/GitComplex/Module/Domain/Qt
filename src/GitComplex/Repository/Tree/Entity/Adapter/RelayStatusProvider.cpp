/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Tree/Entity/Adapter/RelayStatusProvider.h>
#include <GitComplex/Repository/Tree/Entity/Converter/Status.h>
#include <GitComplex/Core/Qt/ActionBuilder.h>
#include <GitComplex/Core/Converter/Path.h>

/*!
 * \file src/GitComplex/Repository/Tree/Entity/Adapter/RelayStatusProvider.cpp
 * \brief src/GitComplex/Repository/Tree/Entity/Adapter/RelayStatusProvider.cpp
 */

namespace GitComplex::Repository::Tree::Entity::Adapter
{

void RelayStatusProvider::requestStatus(
    IdList ids,
    ReceiveAction<Qt::StatusList> receive_action) const
{
    (*m_std_status_provider).requestStatus(
        std::move(ids),
        Core::Qt::makeRelayAction(&m_context,
                                  &RelayStatusProvider::transmitStatuses,
                                  std::move(receive_action)));
}

void RelayStatusProvider::transmitStatuses(
    ReceiveAction<Qt::StatusList> receive_action,
    Std::StatusList statuses)
{
    using Qt::StatusList;
    receive_action(Converter::to<StatusList>(std::move(statuses)));
}

} // namespace GitComplex::Repository::Tree::Entity::Adapter
