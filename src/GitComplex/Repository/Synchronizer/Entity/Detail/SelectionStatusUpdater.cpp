/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Synchronizer/Entity/Detail/SelectionStatusUpdater.h>

/*!
 * \file src/GitComplex/Repository/Synchronizer/Entity/Detail/SelectionStatusUpdater.cpp
 * \brief src/GitComplex/Repository/Synchronizer/Entity/Detail/SelectionStatusUpdater.cpp
 */

namespace GitComplex::Repository::Synchronizer::Entity::Detail
{

SelectionStatusUpdater::SelectionStatusUpdater(
    otn::unique_single<SelectionProvider> selection_provider,
    otn::weak_optional<StatusUpdater>     updater)
    : m_selection_provider{std::move(selection_provider)},
      m_updater{std::move(updater)}
{}

void SelectionStatusUpdater::updateRepositoriesStatus(Selection::Mode mode)
{
    IdList ids = (*m_selection_provider).repositories(mode);

    if (!ids.empty())
        otn::access(m_updater, [&](auto& updater)
                    { updater.updateSuperprojectsStatus(ids); });
}

} // namespace GitComplex::Repository::Synchronizer::Entity::Detail
