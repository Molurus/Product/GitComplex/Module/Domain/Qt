/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Synchronizer/Entity/Detail/StructureAdapter.h>
#include <GitComplex/Core/ActionBuilder.h>

/*!
 * \file src/GitComplex/Repository/Synchronizer/Entity/Detail/StructureAdapter.cpp
 * \brief src/GitComplex/Repository/Synchronizer/Entity/Detail/StructureAdapter.cpp
 */

namespace GitComplex::Repository::Synchronizer::Entity::Detail
{

StructureAdapter::StructureAdapter(
    otn::weak_optional<Tree::StructureObserver> observer,
    otn::raw::weak_single<const IdProvider>     id_provider,
    otn::raw::weak_single<StructureScanner>     scanner)
    : m_observer{std::move(observer)},
      m_id_provider{std::move(id_provider)},
      m_scanner{std::move(scanner)}
{}

void StructureAdapter::addRepositories(PathList paths)
{
    (*m_scanner).scan(paths,
                      makeDirectAction(m_observer,
                                       &Tree::StructureObserver::addRepositories));
}

void StructureAdapter::removeRepositories(PathList paths)
{
    otn::access(m_observer, [&](auto& observer)
                { observer.removeRepositories((*m_id_provider).ids(paths)); });
}

void StructureAdapter::resetRepositories(PathList paths)
{
    if (paths.empty())
    {
        otn::access(m_observer, [&](auto& observer)
                    { observer.resetRepositories(); });
        return;
    }

    addRepositories(std::move(paths));
}

} // namespace GitComplex::Repository::Synchronizer::Entity::Detail
