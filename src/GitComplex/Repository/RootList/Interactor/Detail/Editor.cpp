/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/RootList/Interactor/Detail/Editor.h>

/*!
 * \file src/GitComplex/Repository/RootList/Interactor/Detail/Editor.cpp
 * \brief src/GitComplex/Repository/RootList/Interactor/Detail/Editor.cpp
 */

namespace GitComplex::Repository::RootList::Interactor::Detail
{

Editor::Editor(otn::weak_optional<Observer>    observer,
               otn::raw::weak_single<Provider> add_provider,
               otn::raw::weak_single<Provider> remove_provider)
    : m_observer{std::move(observer)},
      m_add_provider{std::move(add_provider)},
      m_remove_provider{std::move(remove_provider)}
{}

void Editor::addRepositories()
{
    Entity::PathList paths = (*m_add_provider).repositories();

    if (!paths.isEmpty())
        otn::access(m_observer, [&](auto& observer)
                    { observer.addRepositories(paths); });
}

void Editor::removeRepositories()
{
    Entity::PathList paths = (*m_remove_provider).repositories();

    if (!paths.isEmpty())
        otn::access(m_observer, [&](auto& observer)
                    { observer.removeRepositories(paths); });
}

} // namespace GitComplex::Repository::RootList::Interactor::Detail
