/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/RootList/Entity/Detail/Synchronizer.h>

/*!
 * \file src/GitComplex/Repository/RootList/Entity/Detail/Synchronizer.cpp
 * \brief src/GitComplex/Repository/RootList/Entity/Detail/Synchronizer.cpp
 */

// TODO: Move std::invoke specialization out.
namespace std
{

template <class Method, class Object, class ... Args>
invoke_result_t<Method, Object, Args...>
invoke(Method&& method,
       const otn::weak_optional<Object>& object,
       Args&& ... args) noexcept
{
    // TODO: Fix a return on the 'else' branch.
    return otn::access(object, [&](auto& object)
        {
            return std::invoke(std::forward<Method>(method),
                               object,
                               std::forward<Args>(args)...);
        });
}

} // namespace std

namespace GitComplex::Repository::RootList::Entity::Detail
{

namespace
{

FixedObjectConnections<2>
connect(const Observable& observable,
        otn::weak_optional<Observer> observer)
{
    using std::invoke;

    return {
        QObject::connect(
            &observable.repositoriesAdded(),
            &EventDelegate::Observer::repositoriesAdded,
            [ = ](const PathList& paths)
            { invoke(&Observer::addRepositories, observer, paths); }),
        QObject::connect(
            &observable.repositoriesRemoved(),
            &EventDelegate::Observer::repositoriesRemoved,
            [observer = std::move(observer)](const PathList& paths)
            { invoke(&Observer::removeRepositories, observer, paths); })
    };
}

} // namespace

Synchronizer::Synchronizer(const Observable& observable,
                           otn::weak_optional<const Provider> provider,
                           otn::weak_optional<Observer>       observer)
    : m_provider{std::move(provider)},
      m_observer{std::move(observer)},
      m_connections{connect(observable, m_observer)}
{}

void Synchronizer::synchronize() const
{
    otn::access(m_provider, m_observer, [&](auto& provider, auto& observer)
                { observer.resetRepositories(provider.repositories()); });
}

} // namespace GitComplex::Repository::RootList::Entity::Detail
