/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/RootList/Entity/Detail/Storage.h>

namespace GitComplex::Repository::RootList::Entity::Detail
{

Storage::Storage()
    : m_observer_event_delegate{otn::itself},
      m_provider_event_delegate{otn::itself}
{}

void Storage::save(Settings::RepositoryStorage& storage) const
{
    storage.setValue("Repositories", m_repositories);
}

void Storage::load(Settings::RepositoryStorage& storage)
{
    resetRepositories(storage.value("Repositories").toStringList());
}

void Storage::resetRepositories(const PathList& paths)
{
    addRepositories(paths);
}

void Storage::addRepositories(const PathList& paths)
{
    PathList added_paths;
    for (const auto& path : paths)
        if (addRepository(path))
            added_paths.append(path);

    if (!added_paths.isEmpty())
        (*m_observer_event_delegate).repositoriesAdded(added_paths);
}

void Storage::removeRepositories(const PathList& paths)
{
    PathList removed_paths;
    for (const auto& path : paths)
        if (removeRepository(path))
            removed_paths.append(path);

    if (!removed_paths.isEmpty())
        (*m_observer_event_delegate).repositoriesRemoved(removed_paths);
}

bool Storage::addRepository(const Path& path)
{
    if (path.isEmpty() || m_repositories.contains(path))
        return false;

    m_repositories.append(path);

    return true;
}

bool Storage::removeRepository(const Path& path)
{
    if (path.isEmpty() || !m_repositories.contains(path))
        return false;

    return m_repositories.removeOne(path);
}

} // namespace GitComplex::Repository::RootList::Entity::Detail
