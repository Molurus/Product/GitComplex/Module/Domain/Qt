/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Scanner/Entity/Adapter/RelayFileSystem.h>
#include <GitComplex/Repository/Tree/Entity/Converter/Structure.h>
#include <GitComplex/Core/Qt/ActionBuilder.h>
#include <GitComplex/Core/Converter/Path.h>

/*!
 * \file src/GitComplex/Repository/Scanner/Entity/Adapter/RelayFileSystem.cpp
 * \brief src/GitComplex/Repository/Scanner/Entity/Adapter/RelayFileSystem.cpp
 */

namespace GitComplex::Repository::Scanner::Entity::Adapter
{

void RelayFileSystem::scan(
    Core::Qt::PathList paths,
    ReceiveAction<Qt::Structure::List> receive_action) const
{
    (*m_std_structure_scanner).scan(
        Converter::to<Core::Std::PathList>(paths),
        Core::Qt::makeRelayAction(&m_context,
                                  &RelayFileSystem::transmitStructures,
                                  std::move(receive_action)));
}

void RelayFileSystem::transmitStructures(
    ReceiveAction<Qt::Structure::List> receive_action,
    Std::Structure::List structures)
{
    using Qt::Structure;
    receive_action(Converter::to<Structure::List>(std::move(structures)));
}

} // namespace GitComplex::Repository::Scanner::Entity::Adapter
