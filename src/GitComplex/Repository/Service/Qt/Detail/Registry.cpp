/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/Repository/Service/Qt/Detail/Registry.h>
#include <GitComplex/Repository/Entity/Adapter/BlockingIdProvider.h>
#include <GitComplex/Repository/Scanner/Entity/Adapter/RelayFileSystem.h>
#include <GitComplex/Repository/Tree/Entity/Adapter/RelayStatusProvider.h>
#include <GitComplex/Repository/Shell/Entity/Adapter/RelayCommandExecutor.h>
#include <GitComplex/Repository/Shell/Entity/Qt/Concrete/ToolLauncher.h>
#include <GitComplex/Settings/Utility.h>

#include <QCoreApplication>

/*!
 * \file include/GitComplex/Repository/Service/Qt/Detail/Registry.h
 * \brief include/GitComplex/Repository/Service/Qt/Detail/Registry.h
 */

namespace GitComplex::Repository::Service::Qt::Detail
{

using BlockingIdProvider   = Entity::Adapter::BlockingIdProvider;
using RelayFileSystem      = Scanner::Entity::Adapter::RelayFileSystem;
using RelayStatusProvider  = Tree::Entity::Adapter::RelayStatusProvider;
using RelayCommandExecutor = Shell::Entity::Adapter::RelayCommandExecutor;
using ConcreteToolLauncher = Shell::Entity::Qt::Concrete::ToolLauncher;

Registry::Registry(
    otn::raw::weak_single<const Registry::StdIdProvider>       std_id_provider,
    otn::raw::weak_single<const Registry::StdStructureScanner> std_structure_scanner,
    otn::raw::weak_single<const Registry::StdStatusProvider>   std_status_provider,
    otn::raw::weak_single<const Registry::StdCommandExecutor>  std_command_executor)
    : m_id_provider{otn::itself_type<BlockingIdProvider>,
                    std::move(std_id_provider)},
      m_structure_scanner{otn::itself_type<RelayFileSystem>,
                          std::move(std_structure_scanner), *qApp},
      m_status_provider{otn::itself_type<RelayStatusProvider>,
                        std::move(std_status_provider), *qApp},
      m_command_executor{otn::itself_type<RelayCommandExecutor>,
                         std::move(std_command_executor), *qApp},
      m_tool_launcher{otn::itself_type<ConcreteToolLauncher>,
                      m_command_executor}
{}

void Registry::save(Settings::UserStorage& storage) const
{
    Settings::save_pmr(*m_tool_launcher, storage, "ToolLauncher");
}

void Registry::load(Settings::UserStorage& storage)
{
    Settings::load_pmr(*m_tool_launcher, storage, "ToolLauncher");
}

} // namespace GitComplex::Repository::Service::Qt::Detail
