/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <GitComplex/UserSession/Environment.h>
#include <GitComplex/Tool/Std/SuperprojectDetector.h>
#include <GitComplex/Core/Converter/Path.h>

/*!
 * \file src/GitComplex/UserSession/Environment.cpp
 * \brief src/GitComplex/UserSession/Environment.cpp
 */

namespace GitComplex::UserSession
{

Environment::Environment()
{
    Tool::Std::SuperprojectDetector detector;
    using Converter::convert;

    if (!detector.gitDir().empty())
        m_repository_storage_file = convert(detector.gitDir() / "GitComplex.ini");

    m_working_dir_repository = convert(detector.workingTree());
}

Settings::UserStorage Environment::userStorage() const
{
    return {};
}

Settings::RepositoryStorage Environment::repositoryStorage() const
{
    if (!m_repository_storage_file.isEmpty())
        return {m_repository_storage_file, QSettings::IniFormat};

    return {};
}

} // namespace GitComplex::UserSession
