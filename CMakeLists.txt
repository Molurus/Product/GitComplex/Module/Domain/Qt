# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

cmake_minimum_required(VERSION 3.13...3.14.2)

project(GitComplex.Domain.Qt CXX)

include(Molurus.Module)

add_molurus_library(${PROJECT_NAME} 17)
molurus_module_sources(PRIVATE Include Source)
molurus_module_use_automoc()

find_package(Qt5Core)

target_link_libraries(${MODULE_TARGET}
  PUBLIC GitComplex.Domain.Std
  PUBLIC Qt5::Core)

molurus_module_propagate_pic()
